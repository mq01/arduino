String msg="Which LED to turn ON ? RED GREEN or YELLOW? ";
String led;
int redLed=9;
int greenLed=10;
int yellowLed=11;

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
pinMode(redLed,OUTPUT);
pinMode(greenLed,OUTPUT);
pinMode(yellowLed,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(msg);
  while(Serial.available()==0){

  }
  
  led=Serial.readString();

  if(led=="RED" || led=="red"){
   digitalWrite(redLed,HIGH);
   digitalWrite(greenLed,LOW);
   digitalWrite(yellowLed,LOW);
  }

  if(led=="GREEN" || led=="green"){
    digitalWrite(greenLed,HIGH);
    digitalWrite(redLed,LOW);
    digitalWrite(yellowLed,LOW);}
 
  if(led=="YELLOW" || led=="yellow"){
      digitalWrite(yellowLed,HIGH);
      digitalWrite(redLed,LOW);
      digitalWrite(greenLed,LOW);
    }


}
