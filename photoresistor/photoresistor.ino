int lightpin=A1;
float lightval;
int greenled=9;
int redled=10;

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
pinMode(lightpin,INPUT);
pinMode(greenled,OUTPUT);
pinMode(redled,OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
lightval=analogRead(lightpin);
Serial.println(lightval);
delay(500);

if(lightval>=200)
{
  digitalWrite(greenled,HIGH);
  digitalWrite(redled,LOW);
}
else{
   digitalWrite(greenled,LOW);
  digitalWrite(redled,HIGH);
}
}
