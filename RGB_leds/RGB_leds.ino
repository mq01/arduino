int redLed=11;
int greenLed=10;
int blueLed=9;
String msg="Choose a color: ";
String led;

void setup() {
  // put your setup code here, to run once:
 Serial.begin(9600);
 pinMode(redLed,OUTPUT);
 pinMode(greenLed,OUTPUT);
 pinMode(blueLed,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
Serial.println(msg);
while(Serial.available()==0){
}
led=Serial.readString();

if(led=="red" || led=="RED"){
  digitalWrite(redLed,HIGH);
  digitalWrite(greenLed,LOW);
  digitalWrite(blueLed,LOW);
}
if(led=="green" || led=="GREEN"){
  digitalWrite(redLed,LOW);
  digitalWrite(greenLed,HIGH);
  digitalWrite(blueLed,LOW);
}
if(led=="blue" || led=="BLUE"){
  digitalWrite(redLed,LOW);
  digitalWrite(greenLed,LOW);
  digitalWrite(blueLed,HIGH);
}

if(led=="off"){
  digitalWrite(redLed,LOW);
  digitalWrite(greenLed,LOW);
  digitalWrite(blueLed,LOW);
}
if(led=="aqua"){
  digitalWrite(redLed,LOW);
 analogWrite(greenLed,255);
 analogWrite(blueLed,80);
}
if(led=="magenta"){
  digitalWrite(greenLed,LOW);
 analogWrite(redLed,255);
 analogWrite(blueLed,255);
}
}
