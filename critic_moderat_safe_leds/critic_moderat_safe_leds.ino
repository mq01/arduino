int readpin=A0;
float readval;
float v2;
int wait=500;
int yellowled=10;
int greenled=11;
int redled=9;

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
pinMode(readpin,INPUT);
pinMode(yellowled,OUTPUT);
pinMode(greenled,OUTPUT);
pinMode(redled,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  readval=analogRead(readpin);
  v2=(5./1023.)*readval;
  Serial.println(v2);

  if(v2>4.0){
    digitalWrite(redled,HIGH);
     digitalWrite(yellowled,LOW);
     digitalWrite(greenled,LOW);
  }else{if(v2>=3.0 && v2<4.0){
    digitalWrite(redled,LOW);
     digitalWrite(yellowled,HIGH);
     digitalWrite(greenled,LOW);}
     else{
      if(v2<3.0 && v2>0){
       digitalWrite(redled,LOW);
     digitalWrite(yellowled,LOW);
     digitalWrite(greenled,HIGH);}
     }
    
  }
  delay(wait);

}
