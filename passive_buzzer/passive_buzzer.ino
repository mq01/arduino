int buzzPin=9;
int buzzTime2=0;
int potpin=A0;
int potval;
int wait=300;

void setup() {
  // put your setup code here, to run once:
pinMode(buzzPin,OUTPUT);
pinMode(potpin,INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
potval=analogRead(potpin);
buzzTime2=(9940./1023.)*potval;

digitalWrite(buzzPin,HIGH);
delayMicroseconds(buzzTime2);
digitalWrite(buzzPin,LOW);
delayMicroseconds(buzzTime2);
}
